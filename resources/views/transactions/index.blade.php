@extends('layouts.app')

@section('content')
	@can('isAdmin')
		<h2>Transactions</h2>
		<hr>
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  	<li class="nav-item">
		    	<a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">Transactions Pending Approval</a>
		  	</li>
			<li class="nav-item">
		    	<a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Approved Transactions</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">Completed Transactions</a>
		  	</li>
		</ul>
		<div class="tab-content" id="myTabContent">
			{{-- check if a session flash variable containing a notificaion message is set --}}
			@if(session('status'))
				<div class="alert alert-warning" role="alert">
					<h2>Sorry!</h2>
					<p>{{session('status')}}</p>
					<hr>
					<h5>Proceed to <a href="/series/create">Add an asset</a>.</h5>
				</div>
			@endif
	  		<div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
				<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th>User</th>
							<th>Series</th>
							<th>Ref. No.</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Date Requested</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 1)
								<tr>
							    	<td>{{$transaction->user->name}}</td>
								    <td>{{$transaction->series->name}}</td>
								    <td>{{$transaction->refNo}}</td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->created_at}}</td>
								    <td>
							      		<form method="POST" action="/transactions/{{$transaction->id}}">
							      			@csrf
							      			@method('PUT')
							      				<button class="btn btn-success mb-1" type="submit">Approve</button>
							      		</form>
							      		<form method="POST" action="/transactions/{{$transaction->id}}">
							      			@csrf
							      			@method('DELETE')
							      				<button type="submit" class="btn btn-danger">Reject</button>
							      		</form>
							     	</td>
							    </tr>
							@endif
						@endforeach
					</tbody>
				</table>
	  		</div>

	  		<div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
	  			<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th>User</th>
							<th>Transaction</th>
							<th>Asset</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Approved On</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 2)
								<tr>
							    	<td>{{$transaction->user->name}}</td>
								    <td>{{$transaction->refNo}}</td>
								    <td>{{$transaction->asset->modelNo}}</td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->updated_at}}</td>
								    <td>
							      		<form method="POST" action="/transactions/{{$transaction->id}}">
							      			@csrf
							      			@method('PUT')
							      			<button class="btn btn-success mb-1" type="submit">Tag as Returned</button>
							      		</form>
							     	</td>
							    </tr>
							@endif
						@endforeach
					</tbody>
				</table>
	  		</div>

	  		<div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
	  			<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th>Ref. No.</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Completed On</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
								<tr>
									<input type="hidden" name="transaction_id" value="{{$transaction->id}}">
								    <td>{{$transaction->refNo}}</td>
								    <td>
								    	@if($transaction->asset_id == null)
								    		N/A
								    	@else
								    		{{$transaction->asset->modelNo}}
								    	@endif
								    </td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->status->name}}</td>
								    <td>{{$transaction->created_at}}</td>
							    </tr>
						    @endif
						@endforeach
					</tbody>
				</table>
			</div>
	@else
		<div class="jumbotron py-1 my-2">
			<h2>Transactions</h2>
			<hr>
			<h5>Go back to <a href="/series" class="btn btn-outline-primary">Series Catalogue</a></h5>
		</div>
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  	<li class="nav-item">
		    	<a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">My Pending Requests</a>
		  	</li>
			<li class="nav-item">
		    	<a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">My Approved Requests</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">My Completed Requests</a>
		  	</li>
		</ul>
		<div class="tab-content" id="myTabContent">
	  		<div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
				<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th></th>
							<th>Series</th>
							<th>Ref. No.</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Date Requested</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 1)
								<tr>
							    	<td style="width: 10%;"><img class="img-fluid" src="{{$transaction->series->img_path}}"></td>
								    <td>{{$transaction->series->name}}</td>
								    <td>{{$transaction->refNo}}</td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->created_at}}</td>
								    <td>
							      		<form method="POST" action="/transactions/{{$transaction->id}}">
							      			@csrf
							      			@method('PUT')
							      			<button type="submit" class="btn btn-warning" name="cancel" id="cancelBtn">Cancel</button>
							      		</form>
							     	</td>
						    	</tr>
						    @endif
						@endforeach
					</tbody>
				</table>
	  		</div>
	  		<div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
	  			<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th></th>
							<th>Ref. No.</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Approved On</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 2)
								<tr>
								    <td style="width: 10%;"><img class="img-fluid" src="{{$transaction->series->img_path}}"></td>
								    <td>{{$transaction->refNo}}</td>
								    <td>{{$transaction->series->name}}</td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->created_at}}</td>
						    	</tr>
						    @endif
						@endforeach
					</tbody>
				</table>
	  		</div>

	  		<div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
	  			<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th>Ref. No.</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Completed On</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
								<tr>
									<input type="hidden" name="transaction_id" value="{{$transaction->id}}">
								    <td>{{$transaction->refNo}}</td>
								    <td>@if($transaction->asset_id == null)
								    		N/A
								    	@else
								    		{{$transaction->asset->modelNo}}
								    	@endif
								    </td>
								    <td>{{$transaction->borrow_date}}</td>
								    <td>{{$transaction->return_date}}</td>
								    <td>{{$transaction->status->name}}</td>
								    <td>{{$transaction->created_at}}</td>
							    </tr>
						    @endif
						@endforeach
					</tbody>
				</table>
	  		</div>
		</div>
	@endcan
@endsection
