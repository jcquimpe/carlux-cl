@extends('layouts.app')

@section('content')
	@can('isAdmin')

	@else
		<h2>Transactions</h2>
		<hr>
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  	<li class="nav-item">
		    	<a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">My Pending Requests</a>
		  	</li>
			<li class="nav-item">
		    	<a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">My Approved Requests</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">My Completed Requests</a>
		  	</li>
		</ul>
		<div class="tab-content" id="myTabContent">
	  		<div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
				<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th></th>
							<th>Series</th>
							<th>Ref. No.</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Request Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							<tr>
						    	<td style="width: 10%;"><img class="img-fluid" src="{{$transaction->series->img_path}}"></td>
							    <td>{{$transaction->series->name}}</td>
							    <td>{{$transaction->refNo}}</td>
							    <td>{{$transaction->borrow_date}}</td>
							    <td>{{$transaction->return_date}}</td>
							    <td>{{$transaction->created_at}}</td>
							    <td>
						      		<form method="POST" action="/transactions/{{$transaction->id}}">
						      			@csrf
						      			@method('PUT')
						      			<button type="submit" class="btn btn-warning" name="cancel" id="cancelBtn">Cancel</button>
						      		</form>
						     	</td>
						    </tr>
						@endforeach
					</tbody>
				</table>
	  		</div>
	  		<div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab"><h1>Approved</h1></div>

	  		<div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
	  			<table class="table table-hover table-dark">
					<thead>
						<tr>
							<th>Series</th>
							<th>Ref. No.</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Created On</th>
						</tr>
					</thead>
					<tbody>
						@foreach($$cancelled_trans as $transaction)
							<tr>
								<input type="hidden" name="transaction_id" value="{{$transaction->id}}">
							    <td>{{$transaction->series->name}}</td>
							    <td>{{$transaction->refNo}}</td>
							    <td>{{$transaction->borrow_date}}</td>
							    <td>{{$transaction->return_date}}</td>
							    <td>{{$transaction->created_at}}</td>
							    <td></td>
						    </tr>
						@endforeach
					</tbody>
				</table>
	  		</div>
		</div>
	@endcan
@endsection
